const db = require('./db.js');

module.exports = {
    selectRowById: (entityName, id) => db.one("SELECT * FROM $1:name WHERE ID = $2", [entityName, id]),
    getUser: (email) => db.one("SELECT * FROM USERS WHERE EMAIL = $1", [email])
}