const db = require('./db.js');

const { QueryFile } = require('pg-promise');
const { join: joinPath } = require('path');


const fullPath = joinPath(__dirname, "dump.sql");
const dumpSql = new QueryFile(fullPath, { minify: true });

db.none(dumpSql).then(function() {
        console.log("finished");
    })
    .catch(function(error) {
        console.error(error);
    }).finally(() => {
        process.exit();
    });