//https://github.com/vitaly-t/pg-promise
const express = require('express');
const oauth = require('./oauth.js');
const db = require('./db.js');
const cors = require('cors');

const app = express();
const port = 3000;

const router = express.Router();
const contextPath = '/api';

router.use(cors({ origin: '*', port: '*' }));
router.use(express.json());

router.get('/', function(req, res) {
    res.send("PWA API endpoint");
});

const utils = require('./utils.js');

//INTERCEPT HEADER AUTHORIZATION
router.all('/*', function(req, res, next) {
    if (!req.headers.authorization) {
        res.sendStatus(401);
    } else {
        oauth(req.headers.authorization, (user) => {
            //https://stackoverflow.com/questions/18875292/passing-variables-to-the-next-middleware-using-next-in-express-js
            db.one(
                "SELECT * FROM USERS WHERE EMAIL = $1", [user.email]
            ).then(data => {
                db.none("UPDATE USERS SET HEROKU_ID = $1, LOGGED_AT = NOW() WHERE EMAIL = $2", [user.id, data.email]).
                then(() => {
                    utils.getUser(user.email).then(user => {
                        res.locals.user = user;
                        next();
                    });
                });
            }).catch(error => {
                if (error.code == 0) {
                    db.one("INSERT INTO USERS (HEROKU_ID, EMAIL, NAME, LOGGED_AT) VALUES ($1,$2,$3,NOW()) RETURNING ID", [user.heroku_id, user.email, user.name]).
                    then(data => {
                        utils.getUser(user.email).then(user => {
                            res.locals.user = user;
                            next();
                        });
                    });
                }
            });
        }, () => {
            res.sendStatus(401);
        }, (error) => {
            res.send(error, 500);
        });
    }
})

//READ ALL
router.get('/me', (req, res) => {
    res.send(res.locals.user);
});

//READ ALL
router.get('/:entities', (req, res) => {
    db.any("SELECT * FROM $1:name", [req.params.entities]).then(data => {
        res.json(data);
    }).catch(error => {
        res.sendStatus(500).send(error)
    })
});

//READ BY ID
router.get('/:entities/:id', (req, res) => {
    db.one("SELECT * FROM $1:name WHERE ID = $2", [req.params.entities, req.params.id]).then(data =>
        res.json(data)).catch((error) => {
        if (error.code === 0) {
            res.send(404)
        } else {
            res.send(error, 500);
        }
    });
});

//UPDATE
router.patch('/:entities/:id', (req, res) => {
    db.one("SELECT * FROM $1:name WHERE ID = $2", [req.params.entities, req.params.id]).then(data => {
        if (req.params.entities === 'todos') {
            let todosService = require('./todos');
            req.body.id = req.params.id;
            todosService.update(req.body).then(data => res.json(data))
                .catch(error => {
                    res.status(500).send(error);
                });
        }
    }).catch((error) => {
        if (error.code === 0) {
            res.send(404);
        } else {
            res.status(500).send(error);
        };
    });
});

//DELETE
router.delete('/:entities/:id', (req, res) => {
    db.none("DELETE FROM $1:name WHERE ID = $2", [req.params.entities, req.params.id]).then(() =>
        res.json()).catch(error => {
        res.status(500).send(error)
    });
});

//INSERT
router.post('/:entities', (req, res) => {
    if (req.params.entities === 'todos') {
        let todosService = require('./todos');
        let p = null;

        if (Array.isArray(req.body)) {
            p = db.tx(t => {
                let op = [];
                for (todo of req.body) {
                    op.push(todosService.insert(todo, res.locals.user));
                }
                return t.batch(op);
            });
        } else {
            p = todosService.insert(req.body, res.locals.user);
        }

        p.then(data => {
            res.json(data)
        }).catch(
            error => {
                res.status(500).send(error)
            });
    } else {
        res.sendStatus(403);
    }
});

app.use(contextPath, router);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})