const pgp = require('pg-promise')( /* options */ )

const config = {
    username: "",
    password: "",
    host: "",
    port: "",
    database: ""
}

if (!config.username || !config.password || !config.host || !config.port || !config.database) {
    throw Error("You should fill db config in db.js!!!");
}

module.exports = pgp(`postgres://${config.username}:${config.password}@${config.host}:${config.port}/${config.database}?ssl=true`);