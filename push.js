const webpush = require('web-push');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors({ origin: '*' }));
app.use(bodyParser.json());

app.listen(4000, () => {
    console.log('The server started on port 4000 !!!!!!');
});

app.get('/', (req, res) => {
    res.send(
        "hello push server"
    );
});

app.post('/data', (req, res) => {
    console.log('request came');
    const userData = req.body;
    console.log(JSON.stringify(userData));
    res.send(userData);
});

console.log(webpush.generateVAPIDKeys());

const publicKey =
    'BJkUsZs9QYXvusPKt-2SJ_LZD8Y6L3HHEJjP9rVNaiBgjOCr5aLyGMGGJelLMi4eh-2M7SyRbQPZvEok1iYtTuA';
const privateKey = '23muu1by01FPDCNc23fLINamkKGXY7HRfg0KZohghw4';

const sub = {
    "endpoint": "https://fcm.googleapis.com/fcm/send/eR_K3GdSga4:APA91bFdnV7UouTg2qR62PaTriAk0xIbT_sJmu1mYETN7Vwwc6OWTmghktywNoHijr6g5BY2QYohMvYjLwF1frqm7NNRuD7lNQcR4Cq88HjQtSD_3gdNQShT0p4QjbLxRkJn900HoWMV",
    "expirationTime": null,
    "keys": {
        "p256dh": "BCcAPQmD-4iBBHpQEtbyUY-7nnKQTaJQwAjGcRIj0zrimtUykZiIj_eoSUzcIGpY8M6Sov20HvI5Z0dluNLZmWc",
        "auth": "8lPRI23UQHLfyx5293tJzQ"
    }
}

webpush.setVapidDetails('http://localhost:4200/todos', publicKey, privateKey);

const payLoad = {
    notification: {
        data: { url: 'http://localhost:4200/todos' },
        title: 'Todo is expiring....',
        vibrate: [100, 50, 100]
    },
};


webpush.sendNotification(sub, JSON.stringify(payLoad));