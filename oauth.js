const https = require('https');

const herokuOptions = {
    host: 'api.heroku.com',
    port: 443,
    path: '/account',
    method: 'GET',
    headers: {
        'Accept': 'application/vnd.heroku+json; version=3'
    }
}

module.exports = (bearer, onSuccess, onUnauthorized, onError) => {
    let newHerokuOptions = Object.assign({}, herokuOptions);
    newHerokuOptions.headers.Authorization = bearer;
    let output = '';
    const req = https.request(newHerokuOptions, (res) => {
        res.on('data', (chunk) => {
            output += chunk;
        });
        res.on('end', () => {
            console.log(output);
            (res.statusCode == 401) ? onUnauthorized(): onSuccess(JSON.parse(output));
        });
    });
    req.on('error', (err) => {
        onError(err);
    });
    req.end();
}