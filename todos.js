const db = require('./db.js');
const utils = require('./utils.js');

module.exports = {
    insert: (todo, user) => {
        return db.one("INSERT INTO TODOS(DESCRIPTION, EXPIRE_AT, CREATED_AT, USER_ID) VALUES($1,$2,NOW(),$3) RETURNING ID", [todo.description, todo.expireAt, user.id])
            .then(data => {
                return utils.selectRowById('todos', data.id);
            });
    },
    update: (todo) => {
        console.log(todo);
        return db.none("UPDATE todos SET DESCRIPTION = $1, EXPIRE_AT = $2 WHERE ID = $3", [todo.description, todo.expire_at, todo.id]).
        then(() => {
            return utils.selectRowById('todos', todo.id);
        });
    }
}